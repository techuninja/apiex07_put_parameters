package com.techu.productos;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {

    public static List<Producto> productos = new ArrayList<>();

    /*@GetMapping("/v0/productos/{id}")
    public Producto getProductoId1(@PathVariable Integer id){
        Producto producto = new Producto(id);
        return producto;
    }

    @GetMapping("/v1/productos")
    public Producto getProductoId2(@RequestParam(value = "id", defaultValue = "0") Integer id){
        Producto producto = new Producto(id);
        return producto;
    }*/

    @PutMapping("/productos/{id}")
    public Producto updateProducto(@PathVariable Integer id, @RequestBody Producto producto){
        //System.out.println(producto);
        //productos.add(producto);
        return producto;
    }
}
